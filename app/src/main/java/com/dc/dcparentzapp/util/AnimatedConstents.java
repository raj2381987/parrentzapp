package com.dc.dcparentzapp.util;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.OvershootInterpolator;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;

import com.dc.dcparentzapp.R;
import com.dc.dcparentzapp.constants.AppConstants;

/**
 * Created by rpeela on 10/16/15.
 */
public class AnimatedConstents {


    public static final DecelerateInterpolator DECCELERATE_INTERPOLATOR = new DecelerateInterpolator();
    public static final AccelerateInterpolator ACCELERATE_INTERPOLATOR = new AccelerateInterpolator();
    public static final OvershootInterpolator OVERSHOOT_INTERPOLATOR = new OvershootInterpolator(4);
    public static final Interpolator INTERPOLATOR = new DecelerateInterpolator();

    public static void setListAnimation(View view, int position, int displayHeight) {
        TranslateAnimation translation = new TranslateAnimation(0f, 0F, -1 * displayHeight, 0f);
        translation.setStartOffset(0);
        float value = AppConstants.MENU_ITEMS.length - position;
        value += 0.3;
        translation.setDuration((long) (value * 200));
        translation.setFillAfter(true);
//        translation.setInterpolator(new BounceInterpolator());
        view.setAnimation(translation);
    }

    public static void setTranslateAnimation(View view, int displayHeight, final AnimationEndListener animationEndListener) {
        TranslateAnimation translation = new TranslateAnimation(0f, 0F, -1 * displayHeight, 0f);
        translation.setStartOffset(0);
        translation.setDuration(1000);
        translation.setFillAfter(true);
//        translation.setInterpolator(new BounceInterpolator());
        view.setAnimation(translation);
        translation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                animationEndListener.onAnimationEnded();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    public interface AnimationEndListener {
        public void onAnimationEnded();
    }


    public static void scaleView(View v, float startScale, float endScale) {
        ScaleAnimation animation = new ScaleAnimation(0, 1f, 0, 1f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        animation.setFillAfter(true); // Needed to keep the result of the animation
        animation.setDuration(500);
        v.startAnimation(animation);
    }

    public static void flipAnimation(Context context, View v, int duration) {
        AnimatorSet shrinkSet = (AnimatorSet) AnimatorInflater.loadAnimator(context, R.animator.card_flip_left_in);
        shrinkSet.setDuration(duration);
        shrinkSet.setTarget(v);
        shrinkSet.start();

    }


    public static void rotateAnimateIcon(View view, long startDelay) {
        AnimatorSet animatorSet = new AnimatorSet();

        ObjectAnimator rotationAnim = ObjectAnimator.ofFloat(view, "rotation", 0f, 360f);
        rotationAnim.setDuration(300);
        rotationAnim.setInterpolator(ACCELERATE_INTERPOLATOR);

        ObjectAnimator bounceAnimX = ObjectAnimator.ofFloat(view, "scaleX", 0.2f, 1f);
        bounceAnimX.setDuration(300);
        bounceAnimX.setInterpolator(OVERSHOOT_INTERPOLATOR);

        ObjectAnimator bounceAnimY = ObjectAnimator.ofFloat(view, "scaleY", 0.2f, 1f);
        bounceAnimY.setDuration(300);
        bounceAnimY.setInterpolator(OVERSHOOT_INTERPOLATOR);
        bounceAnimY.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
//                holder.btnLike.setImageResource(R.drawable.ic_heart_red);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
//                heartAnimationsMap.remove(holder);
//                dispatchChangeFinishedIfAllAnimationsEnded(holder);
            }
        });

        animatorSet.play(bounceAnimX).with(bounceAnimY).after(rotationAnim);
        animatorSet.setStartDelay(startDelay);
        animatorSet.start();
    }

}
