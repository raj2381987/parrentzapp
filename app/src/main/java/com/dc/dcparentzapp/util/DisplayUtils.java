package com.dc.dcparentzapp.util;

import android.content.Context;
import android.util.DisplayMetrics;
import android.util.TypedValue;

/**
 * Created by Raja.p on 23-02-2016.
 */
public class DisplayUtils {

    private static DisplayUtils instance = null;
    public final float width;
    public final float height;
    private int actionBarHeight;
    private DisplayMetrics metrics;

    private DisplayUtils(Context context) {
        metrics = context.getResources().getDisplayMetrics();
        this.width = metrics.widthPixels;
        this.height = metrics.heightPixels;
        TypedValue tv = new TypedValue();
        if (context.getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
            actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, context.getResources().getDisplayMetrics());
        }

    }

    public float getWidth() {
        return width;
    }

    public static DisplayUtils get(Context context) {
        if (instance == null)
            instance = new DisplayUtils(context);
        return instance;
    }


    public float getHeight() {
        return height;
    }

    public int dpToPx(int dp) {
        return Math.round(dp * (metrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }


    public int getActionBarHeight() {
        return actionBarHeight;
    }
}
