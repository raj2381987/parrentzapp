package com.dc.dcparentzapp.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;
import android.util.Log;


import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by raja on 21/08/17.
 */

public class EncryptedSharedUtils {

    private static String SHARED_PREFERENCE_KEY = "com.capillary.functional.EncryptedSharedUtils";

    private static final String TRANSFORMATION = "AES/CBC/PKCS5Padding";
    private static final String KEY_TRANSFORMATION = "AES/ECB/PKCS5Padding";
    private static final String SECRET_KEY_HASH_TRANSFORMATION = "SHA-256";
    private static final String CHARSET = "UTF-8";

    private static Cipher writer;
    private static Cipher reader;
    private static Cipher keyWriter;
    private static EncryptedSharedUtils instance;
    private Context context;

    public EncryptedSharedUtils(Context context) {
        this.context = context;
    }

    private static class SecurePreferencesException extends RuntimeException {
        private SecurePreferencesException(Throwable e) {
            super(e);
        }
    }


    public static EncryptedSharedUtils get(Context context) {
        if (instance == null)
            instance = new EncryptedSharedUtils(context);
        return instance;
    }

    private SharedPreferences getSharedPreference(Context context) {
        try {
            writer = Cipher.getInstance(TRANSFORMATION);
            reader = Cipher.getInstance(TRANSFORMATION);
            keyWriter = Cipher.getInstance(KEY_TRANSFORMATION);
            try {
                initCiphers(TRANSFORMATION);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (InvalidKeyException e) {
                e.printStackTrace();
            } catch (InvalidAlgorithmParameterException e) {
                e.printStackTrace();
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }
        return context.getSharedPreferences(SHARED_PREFERENCE_KEY, Context.MODE_PRIVATE);
    }

    private void initCiphers(String secureKey) throws UnsupportedEncodingException, NoSuchAlgorithmException, InvalidKeyException,
            InvalidAlgorithmParameterException {
        IvParameterSpec ivSpec = getIv();
        SecretKeySpec secretKey = getSecretKey(secureKey);

        writer.init(Cipher.ENCRYPT_MODE, secretKey, ivSpec);
        reader.init(Cipher.DECRYPT_MODE, secretKey, ivSpec);
        keyWriter.init(Cipher.ENCRYPT_MODE, secretKey);
    }

    private static IvParameterSpec getIv() {
        byte[] iv = new byte[writer.getBlockSize()];
        System.arraycopy("fldsjfodasjifudslfjdsaofshaufihadsf".getBytes(), 0, iv, 0, writer.getBlockSize());
        return new IvParameterSpec(iv);
    }

    private SecretKeySpec getSecretKey(String key) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        byte[] keyBytes = createKeyBytes(key);
        return new SecretKeySpec(keyBytes, TRANSFORMATION);
    }

    private byte[] createKeyBytes(String key) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance(SECRET_KEY_HASH_TRANSFORMATION);
        md.reset();
        byte[] keyBytes = md.digest(key.getBytes(CHARSET));
        return keyBytes;
    }

    private String encrypt(String value, Cipher writer) throws SecurePreferencesException {
        byte[] secureValue;
        try {
            secureValue = convert(writer, value.getBytes(CHARSET));
        } catch (UnsupportedEncodingException e) {
            throw new SecurePreferencesException(e);
        }
        String secureValueEncoded = Base64.encodeToString(secureValue, Base64.NO_WRAP);
        return secureValueEncoded;
    }

    protected String decrypt(String securedEncodedValue) {
        byte[] securedValue = Base64.decode(securedEncodedValue, Base64.NO_WRAP);
        byte[] value = convert(reader, securedValue);
        try {
            return new String(value, CHARSET);
        } catch (UnsupportedEncodingException e) {
            throw new SecurePreferencesException(e);
        }
    }

    private byte[] convert(Cipher cipher, byte[] bs) throws SecurePreferencesException {
        try {
            return cipher.doFinal(bs);
        } catch (Exception e) {
            throw new SecurePreferencesException(e);
        }
    }


    /**
     * It will put the String data to the persistent store.
     *
     * @param key
     * @param value
     */
    public void putValue(String key, String value) {
        try {
            SharedPreferences.Editor editor = getSharedPreference(context).edit();
            String secureValueEncoded = encrypt(value, writer);
            editor.putString(key, secureValueEncoded).apply();
        } catch (Exception e) {
            Log.e("EncryptedSharedUtils", e.getMessage());
        }
    }


    public void removeValues(String... keys) {
        SharedPreferences.Editor edit = getSharedPreference(context).edit();
        for (String key : keys)
            edit.remove(key);
        edit.apply();
    }


    public String getValue(String key) {
        try {
            String securedEncodedValue = getSharedPreference(context).getString(key, null);
            if (securedEncodedValue != null)
                return decrypt(securedEncodedValue);
            else
                return null;
        } catch (Exception e) {
            Log.e("EnSharedUtils:getValue", e.getMessage());
        }
        return null;
    }


}
