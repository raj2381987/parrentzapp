package com.dc.dcparentzapp.util;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import com.dc.dcparentzapp.fragment.dialog.BaseFragmentDialog;
import com.dc.dcparentzapp.fragment.dialog.LoadingDialog;
import com.dms.datalayerapi.util.GetUrlMaker;

/**
 * Created by raja on 31/08/17.
 */

public class Util {
    public static void hideKeyBoard(FragmentActivity activity) {
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromInputMethod(view.getWindowToken(), InputMethodManager.RESULT_HIDDEN);
            activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        }
    }

    public static void logout(Context context) {
        EncryptedSharedUtils.get(context).removeValues(SharedUtil.KEY_ACCESS_CODE, SharedUtil.KEY_PASS_CODE, SharedUtil.KEY_SCHOOL_CODE);
        SharedUtil.get(context).remove(SharedUtil.IS_LOGIN);
        SharedUtil.get(context).remove(SharedUtil.SCHOOL_LOGO);
    }

    public static float dpToPixels(int dp, Context context) {
        return dp * (context.getResources().getDisplayMetrics().density);
    }

    public static BaseFragmentDialog showBaseLoading(FragmentActivity fragmentActivity) {
        LoadingDialog dFragment = new LoadingDialog();
        dFragment.show(fragmentActivity.getSupportFragmentManager(), "Dialog Fragment");
        return dFragment;
    }

    public static GetUrlMaker getCommonGetPerms(Context context) {
        return GetUrlMaker.getMaker().addParams("username", EncryptedSharedUtils.get(context).getValue(SharedUtil.KEY_ACCESS_CODE))
                .addParams("password", EncryptedSharedUtils.get(context).getValue(SharedUtil.KEY_PASS_CODE))
                .addParams("schoolcode", EncryptedSharedUtils.get(context).getValue(SharedUtil.KEY_SCHOOL_CODE));
    }

    public static String recoverImageUrl(String path) {
        if (path != null)
            path = path.replace(" ", "%20");
        if (!path.startsWith("http"))
            path = "http://" + path;
        return path;
    }
}
