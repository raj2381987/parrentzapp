package com.dc.dcparentzapp.util;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.dc.dcparentzapp.R;
import com.dc.dcparentzapp.custom.PercentageImageView;

import java.lang.ref.WeakReference;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

/**
 * Created by raja on 10/05/17.
 */

public class GlideImageUtil {


    private Context context;
    private String url;
    private ScaleType scaleType;
    private int placeHolder = 0;
    private int errorDrawable = 0;

    private DiskCacheStrategy diskCacheStrategy = DiskCacheStrategy.AUTOMATIC;
    private WeakReference<ImageView> imageViewWeakReference = null;
    private boolean isUrlResizeRequired = false;
    public static final String IMAGE_WIDTH_HERE = "*IMAGE_WIDTH*";
    public static final String IMAGE_HEIGHT_HERE = "*IMAGE_HEIGHT*";
    public static final String IMAGE_RESIZE_URL_POSTFIX = ";width=" + IMAGE_WIDTH_HERE + ";height=" + IMAGE_HEIGHT_HERE + ";scale=none;anchor=bottomcenter;";
    private boolean dontAnimate = true;

    public boolean isAnimationRequired() {
        return isAnimationRequired;
    }

    public GlideImageUtil setAnimationRequired(boolean animationRequired) {
        isAnimationRequired = animationRequired;
        return this;
    }

    private boolean isAnimationRequired = true;
    private int widthRatio;
    private int heightRatio;
    private boolean randomBgRequired = true;
    private int[] colors = null;

    public GlideImageUtil(Context context) {
        this.context = context;
    }

    public static GlideImageUtil get(Context context) {
        return new GlideImageUtil(context);
    }

    public GlideImageUtil setScaleType(ScaleType scaleType) {
        this.scaleType = scaleType;
        return this;
    }

    public GlideImageUtil setDontAnimate(boolean dontAnimate) {
        this.dontAnimate = dontAnimate;
        return this;
    }

    public DiskCacheStrategy getDiskCacheStrategy() {
        return diskCacheStrategy;
    }

    public GlideImageUtil setDiskCacheStrategy(DiskCacheStrategy diskCacheStrategy) {
        this.diskCacheStrategy = diskCacheStrategy;
        return this;
    }


    public enum ScaleType {
        TYPE_THUMBNAIL(4f), TYPE_SHOWCASE(3f), TYPE_PRODUCT_DETAIL(2f), TYPE_FULL_SCREEN(1f);

        private float value;

        ScaleType(float value) {
            this.value = value;
        }

        public float getValue() {
            return value;
        }
    }

    public ScaleType getScaleType() {
        return scaleType;
    }

    public GlideImageUtil setPlaceHolder(int placeHolder) {
        this.placeHolder = placeHolder;
        return this;
    }


    public GlideImageUtil setImageView(ImageView imageView) {
        imageViewWeakReference = new WeakReference<ImageView>(imageView);
        return this;
    }

    public GlideImageUtil isUrlResizeRequired(boolean isUrlResizeRequired) {
        this.isUrlResizeRequired = isUrlResizeRequired;
        return this;
    }


    /**
     * <p>
     * Ratio will be caulated based on the Screen size.
     * <p/>
     * <br></br>
     * If you pass it as 0. Then fetch the full size of image
     *
     * @param widthRatio
     * @param heightRatio <p>Height Ratio Always calculated Based on the width ratio</p>
     * @return
     */
    public GlideImageUtil setRatio(int widthRatio, int heightRatio) {
        this.widthRatio = widthRatio;
        this.heightRatio = heightRatio;
        return this;
    }


    public GlideImageUtil setRandomBgRequired(boolean randomBgRequired) {
        this.randomBgRequired = randomBgRequired;
        return this;
    }

    public void load() {

        String imageUrl = getUrl();

        final ImageView imageView = imageViewWeakReference.get();
        if (imageView != null) {

            int width = imageView.getWidth();
            int height = imageView.getHeight();

            if (imageView instanceof PercentageImageView) {
                imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                if (width == 0 || height == 0) {
                    width = (int) (DisplayUtils.get(context).getWidth() / 2);
                    height = (int) (((PercentageImageView) imageView).getHeightRatio() * width);
                }
            }


            if (widthRatio != 0) {
                width = (int) (DisplayUtils.get(context).getWidth() / width);
                if (heightRatio != 0) {
                    height = (int) (heightRatio * width);
                }
            }


            if (isUrlResizeRequired && width != 0 && height != 0) {
                imageUrl = imageUrl + (IMAGE_RESIZE_URL_POSTFIX.replace(IMAGE_WIDTH_HERE, width + "")).replace(IMAGE_HEIGHT_HERE, height + "");
            }

            RequestManager glideMgr = Glide.with(context);
            RequestBuilder<Drawable> requestBuilder = glideMgr.load(imageUrl);

            RequestOptions options = new RequestOptions();

            options.diskCacheStrategy(diskCacheStrategy);

            if (placeHolder != 0)
                options.placeholder(placeHolder);

            if (errorDrawable != 0)
                options.error(placeHolder);

            if (dontAnimate)
                options.dontAnimate();


            requestBuilder.listener(new RequestListener<Drawable>() {
                @Override
                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target target, boolean isFirstResource) {
                    if (imageView != null && errorDrawable != 0)
                        imageView.setImageResource(errorDrawable);
                    return false;
                }



                @Override
                public boolean onResourceReady(Drawable resource, Object model, Target target, DataSource dataSource, boolean isFirstResource) {
                    if (imageView != null) {
                        imageView.setImageDrawable(resource);
                        if ((imageView.getAnimation() == null || (imageView.getAnimation() != null && imageView.getAnimation().hasEnded())) && isAnimationRequired)
                            imageView.startAnimation(AnimationUtils.loadAnimation(context, R.anim.cap_image_anim));
                    }
                    return false;
                }
            });

            requestBuilder.transition(withCrossFade());

            requestBuilder.apply(options);

            requestBuilder.into(imageView);
        }

    }

    private boolean isRandomBGRequired() {
        return randomBgRequired;
    }


    public GlideImageUtil setRandomColors(int[] colors) {
        this.colors = colors;
        return this;
    }


    public GlideImageUtil setErrorHolder(int drawable) {
        this.errorDrawable = drawable;
        return this;
    }


    public String getUrl() {
        return url;
    }

    public GlideImageUtil setUrl(String url) {
        this.url = url;
        return this;
    }
}
