package com.dc.dcparentzapp;

import android.os.Bundle;

import com.dc.dcparentzapp.constants.AppConstants;
import com.dc.dcparentzapp.model.PaymentInfo;

public class FeePaymentsActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_activity);
        initGUI();
        initData();
    }

    private void initData() {

    }

    private void initGUI() {
        initToolBar("Fee Payments", R.layout.content_view);
        fetchNUpdateDB(AppConstants.GET_PAYMENTS, PaymentInfo.class);
    }

    @Override
    protected <T> void updateOnDB(T t) {
        PaymentInfo paymentInfo = (PaymentInfo) t;


    }
}
