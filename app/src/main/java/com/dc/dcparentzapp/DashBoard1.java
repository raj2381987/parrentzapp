package com.dc.dcparentzapp;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.dc.dcparentzapp.adaptor.DashBoardGridAdapter;
import com.dc.dcparentzapp.adaptor.SiblingCardFragmentPagerAdapter;
import com.dc.dcparentzapp.constants.AppConstants;
import com.dc.dcparentzapp.fragment.SiblingCardFragment;
import com.dc.dcparentzapp.util.DisplayUtils;
import com.dc.dcparentzapp.util.GlideImageUtil;
import com.dc.dcparentzapp.util.ShadowTransformer;
import com.dc.dcparentzapp.util.SharedUtil;
import com.dc.dcparentzapp.util.Util;

import java.util.List;

public class DashBoard1 extends BaseActivity {

    private RecyclerView mRecyclerView;
    private GridLayoutManager mLayoutManager;
    private DrawerLayout drawer_layout;
    private LinearLayout rootLeftNav;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board1);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        initGUI();
        initData();
    }

    private void initData() {

    }


    private void updateSiblings() {

    }


    private void initGUI() {
        initRecyclerView();
        drawer_layout = (DrawerLayout) findViewById(R.id.drawer_layout);
        rootLeftNav = (LinearLayout) findViewById(R.id.leftNavRoot);
        findViewById(R.id.nav_action_menu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!drawer_layout.isDrawerOpen(Gravity.START)) {
                    drawer_layout.openDrawer(Gravity.START);
                    startAnim();
                } else {
                    drawer_layout.closeDrawer(Gravity.START);
                }
            }


        });
        findViewById(R.id.logout_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Util.logout(DashBoard1.this);
                finishAffinity();
                startActivity(new Intent(DashBoard1.this, LoginActivity.class));
            }
        });
        initLeftDrawer();
        initSiblingsView();
        initAnimations();
        initToolBar("Calender");
        initMenuItemClicks(R.id.menu_profile_layout, R.id.profile_img, R.drawable.profile_account, ProfileActivity.class);
    }

    private void initMenuItemClicks(int menu_id, final int imageId, final int icon, final Class<?> activityClass) {
        findViewById(menu_id).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent;
                intent = new Intent(DashBoard1.this, activityClass);
                intent.putExtra("icon", icon);
                intent.putExtra("position", imageId);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    Pair<View, String> p1 = Pair.create(findViewById(imageId), "center_icon" + imageId);
                    ActivityOptionsCompat aoc = ActivityOptionsCompat.makeSceneTransitionAnimation(DashBoard1.this, p1);
                    startActivity(intent, aoc.toBundle());
                } else {
                    startActivity(intent);
                }
            }
        });
    }

    private void initLeftDrawer() {
        String schoolLogo = SharedUtil.get(this).getString(SharedUtil.SCHOOL_LOGO);
        if (schoolLogo != null) {
            try {
                ImageView school_icon = (ImageView) findViewById(R.id.school_icon);
                GlideImageUtil.get(this).setUrl(schoolLogo).setImageView(school_icon).load();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void initSiblingsView() {
        final ViewPager viewPager = (ViewPager) findViewById(R.id.siblingsViewPage);
        DisplayUtils display = DisplayUtils.get(this);
        int padding = (int) (display.width / 10);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            viewPager.setPaddingRelative((2 * padding + padding / 2), 10, (2 * padding + padding / 2), 10);
        } else {
            viewPager.setPadding((2 * padding + padding / 2), 10, (2 * padding + padding / 2), 10);
        }

        SiblingCardFragment.PositionChangeListener posListener = new SiblingCardFragment.PositionChangeListener() {
            @Override
            public void onPositionChanged(int position) {
                viewPager.setCurrentItem(position);
            }
        };


        final SiblingCardFragmentPagerAdapter pagerAdapter = new SiblingCardFragmentPagerAdapter(DashBoard1.this, getSupportFragmentManager(), posListener);
        ShadowTransformer fragmentCardShadowTransformer = new ShadowTransformer(viewPager, pagerAdapter);
        fragmentCardShadowTransformer.enableScaling(true);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                List<SiblingCardFragment> fragments = pagerAdapter.getAllFragments();
                for (int i = 0; i < fragments.size(); i++) {
                    SiblingCardFragment siblingCardFragment = fragments.get(i);
                    siblingCardFragment.setEnabled(i == position);
                    pagerAdapter.getSelectedFragment(position);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        viewPager.setAdapter(pagerAdapter);
        viewPager.setPageTransformer(false, fragmentCardShadowTransformer);
        viewPager.setOffscreenPageLimit(3);
    }

    private void startAnim() {
        for (int i = 0; i < rootLeftNav.getChildCount(); i++) {
            Animation anim = AnimationUtils.loadAnimation(this, R.anim.slide_in_left);
            anim.setDuration(((i + 1) * 50) + 400);
            anim.setStartTime(i * 600);
            rootLeftNav.getChildAt(i).startAnimation(anim);
        }
    }

    private void initAnimations() {
//        AnimatedConstents.setTranslateAnimation(findViewById(R.id.diagonalLayout), 200, new AnimatedConstents.AnimationEndListener() {
//            @Override
//            public void onAnimationEnded() {
//                findViewById(R.id.school_icon).setVisibility(View.VISIBLE);
//                AnimatedConstents.scaleView(findViewById(R.id.school_icon), 0f, 1f);
//            }
//        });
    }

    private void initRecyclerView() {
        mRecyclerView = (RecyclerView) findViewById(R.id.grid_recycle);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new GridLayoutManager(this, 3);
        mRecyclerView.setLayoutManager(mLayoutManager);
        DashBoardGridAdapter mAdapter = new DashBoardGridAdapter(this, AppConstants.getLeftMenuElements(this));
        mRecyclerView.setAdapter(mAdapter);
    }

    private void initToolBar(String calender) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.findViewById(R.id.parrentz_icon).setTransitionName("parrentz_icon");
            toolbar.findViewById(R.id.p_icon).setTransitionName("p_icon");
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
