package com.dc.dcparentzapp;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.dc.dcparentzapp.fragment.AttendanceFragment;
import com.p_v.flexiblecalendar.FlexibleCalendarView;

import java.util.ArrayList;
import java.util.List;

public class CalendarActivity extends BaseActivity {

    private FlexibleCalendarView calendarView;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_activity);

        initToolBar("School Calender", R.layout.calender_content_view);
        initUI();
//        initNewUI();
//        initData();

    }

    private void initUI() {
        TabLayout tabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
        viewPager = (ViewPager) findViewById(R.id.pager);
        TabsPagerAdapter mAdapter = new TabsPagerAdapter(this, getSupportFragmentManager(), 0);
        mAdapter.addFragment(new AttendanceFragment(), "Calender");
        mAdapter.addFragment(new AttendanceFragment(), "Attendance");
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setAdapter(mAdapter);

    }



    @Override
    protected <T> void updateOnDB(T t) {
        super.updateOnDB(t);
    }



    public class TabsPagerAdapter extends FragmentStatePagerAdapter {

        private Context mContext;
        private int TabNumber;
        private List<Fragment> mFragments = new ArrayList<>();
        private List<String> mFragmentTitles = new ArrayList<>();

        public TabsPagerAdapter(Context context, FragmentManager fm, int TabNumber) {
            super(fm);
            this.mContext = context;
            this.TabNumber = TabNumber;
        }

        public void addFragment(Fragment fragment, String title) {
            mFragments.add(fragment);
            mFragmentTitles.add(title);
        }

        public String getTitleName(int position) {
            return mFragmentTitles.get(position);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitles.get(position);
        }

        public View getTabView(int position) {
            View tab = LayoutInflater.from(mContext).inflate(R.layout.tab_item, null);
            TextView tabText = (TextView) tab.findViewById(R.id.tab_text);
            tabText.setText(mFragmentTitles.get(position));
            if (position == TabNumber) {
                tab.setSelected(true);
            }
            return tab;
        }

    }

}
