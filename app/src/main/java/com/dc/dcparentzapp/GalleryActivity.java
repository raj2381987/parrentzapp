package com.dc.dcparentzapp;

import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.dc.dcparentzapp.adaptor.GalleryAdapter;
import com.dc.dcparentzapp.constants.AppConstants;
import com.dc.dcparentzapp.db.DBUtil;
import com.dc.dcparentzapp.model.GalleryInfo;
import com.dc.dcparentzapp.util.SharedUtil;

import java.util.ArrayList;

public class GalleryActivity extends BaseActivity {

    private RecyclerView recyclerView;
    private GalleryAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_activity);
        initGUI();
        initData();
    }

    private void initData() {
        mAdapter = new GalleryAdapter(this, new ArrayList<GalleryInfo.PhotoEvents>());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        updateFromDB();
        fetchNUpdateDB(AppConstants.GET_GALLERY, GalleryInfo.class);
    }

    private void initGUI() {
        initToolBar("Gallery", R.layout.gallery_content_view);
        recyclerView = (RecyclerView) findViewById(R.id.gallery_recyclerView);

    }

    private void updateFromDB() {
        if (DBUtil.get(this).getCountFromTable(GalleryInfo.PhotoEvents.class, "studentId = '" + SharedUtil.get(this).getString(SharedUtil.KEY_ACTIVE_USER_ID) + "'") > 0) {
            ArrayList<GalleryInfo.PhotoEvents> photoEvents = DBUtil.get(this).getAllValuesFromTable(GalleryInfo.PhotoEvents.class, "studentId = '" + SharedUtil.get(this).getString(SharedUtil.KEY_ACTIVE_USER_ID) + "'");
            updateUI(photoEvents);
        }
    }

    private void updateUI(ArrayList<GalleryInfo.PhotoEvents> photoEvents) {
        mAdapter.setMoviesList(photoEvents);
    }

    @Override
    protected <T> void updateOnDB(T t) {
        GalleryInfo galleryInfo = (GalleryInfo) t;
        if (galleryInfo != null && galleryInfo.StudentInfo != null && galleryInfo.StudentInfo.size() > 0) {
            DBUtil.get(this).dropTable(GalleryInfo.PhotoEvents.class);
            for (GalleryInfo.Galleries galleries : galleryInfo.StudentInfo) {
                if (galleries != null && galleries.gallery != null && galleries.gallery.size() > 0) {
                    for (GalleryInfo.Gallery gallery : galleries.gallery) {
                        if (gallery != null && gallery.eventPhotos != null && gallery.eventPhotos.size() > 0) {
                            for (GalleryInfo.PhotoEvents photoEvents : gallery.eventPhotos) {
                                photoEvents.eventName = gallery.eventName;
                                photoEvents.studentId = galleries.studentId;
                            }
                            DBUtil.get(this).bulkInsertion(gallery.eventPhotos);
                        }

                    }
                }
            }
        }
        updateFromDB();
    }
}