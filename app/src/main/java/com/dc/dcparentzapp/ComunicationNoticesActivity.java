package com.dc.dcparentzapp;

import android.os.Bundle;

public class ComunicationNoticesActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_activity);
        initGUI();
        initData();
    }

    private void initData() {

    }

    private void initGUI() {
        initToolBar("Communication/Notice", R.layout.content_view);

    }
}
