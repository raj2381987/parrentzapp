package com.dc.dcparentzapp.constants;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.app.Fragment;

import com.dc.dcparentzapp.AcademicActivity;
import com.dc.dcparentzapp.CalendarActivity;
import com.dc.dcparentzapp.ComunicationNoticesActivity;
import com.dc.dcparentzapp.DigitalCampusCornerActivity;
import com.dc.dcparentzapp.FeePaymentsActivity;
import com.dc.dcparentzapp.GalleryActivity;
import com.dc.dcparentzapp.HomeWorkActivity;
import com.dc.dcparentzapp.PlannerActivity;
import com.dc.dcparentzapp.R;
import com.dc.dcparentzapp.TransportTrackerActivity;
import com.dc.dcparentzapp.fragment.BaseFragment;
import com.dc.dcparentzapp.fragment.ContentFragment;
import com.dc.dcparentzapp.model.MenuItem;

import java.util.ArrayList;

/**
 * Created by rpeela on 10/15/15.
 */
public class AppConstants {

    public static String BASE_URL = "http://digitalcampus.in/ServicesParentZApp/";
    public static String LOGIN_URL = BASE_URL + "Login/parentapp_login.jsp";
    public static String HOLIDAY_URL = BASE_URL + "Calender/HOLIDAYS.jsp";
    public static String GET_SIBLING = BASE_URL + "Login/siblings.jsp";
    public static String GET_GALLERY = BASE_URL + "Calender/GALLERY.jsp";
    public static String GET_PAYMENTS = BASE_URL + "fee/parent_fee.jsp";


    public static int SPLASH_SCREEN_TIME = 5 * 1000;// 5 Sec

    public static String[] MENU_ITEMS = {"School Calendar",
            "Planner", "Academic", "Home Work / Diary", "Fee Payment", "Transport Tracker",
            "Communication / Notices", "Digital Campus Corner", "Gallery"};
    public static int[] MENU_ITEMS_ICONS = {R.drawable.school_cal, R.drawable.planner_icon,
            R.drawable.acadamic_icon, R.drawable.home_work_icon, R.drawable.fee_payment_icon,
            R.drawable.transport_tracker_icon, R.drawable.comunication_icon, R.drawable.capus_corner_icon,
            R.drawable.gallery_icon};

    public static int[] MENU_ITEM_COLORS = {Color.parseColor("#e85d5d"),
            Color.parseColor("#801f59"),
            Color.parseColor("#9B26AF"),
            Color.parseColor("#009688"),
            Color.parseColor("#607D8B"),
            Color.parseColor("#b39136"),
            Color.parseColor("#a84365"),
            Color.parseColor("#795548"),
            Color.parseColor("#795548")
    };


    public static ArrayList<MenuItem> getLeftMenuElements(Context context) {
        ArrayList<MenuItem> menuItems = new ArrayList<MenuItem>();
        for (int i = 0; i < MENU_ITEMS.length; i++) {
            menuItems.add(new MenuItem(MENU_ITEMS[i], MENU_ITEMS_ICONS[i], MENU_ITEM_COLORS[i]));
        }
        return menuItems;
    }

    public static Fragment getPositionFragment(int positions) {

        switch (positions) {
            case 0:
                return ContentFragment.getInstance(positions);
            default:
                return ContentFragment.getInstance(positions);
        }
    }

    public static int getFragmentPosition(BaseFragment fragment) {
        ArrayList<Class> allFragments = new ArrayList<>();
        allFragments = loadAllClass();
        for (int i = 0; i < allFragments.size(); i++) {
            if (allFragments.get(i).isInstance(fragment))
                return i;
        }
        return 0;
    }

    private static ArrayList<Class> loadAllClass() {
        ArrayList<Class> allFragments = new ArrayList<>();
        allFragments.add(ContentFragment.class);
        return allFragments;
    }

    public static Class<?> getActivityName(int position) {
        switch (position) {
            case 0:
                return CalendarActivity.class;
            case 1:
                return PlannerActivity.class;
            case 2:
                return AcademicActivity.class;
            case 3:
                return HomeWorkActivity.class;
            case 4:
                return FeePaymentsActivity.class;
            case 5:
                return TransportTrackerActivity.class;
            case 6:
                return ComunicationNoticesActivity.class;
            case 7:
                return DigitalCampusCornerActivity.class;
            case 8:
                return GalleryActivity.class;
        }
        return null;
    }
}
