package com.dc.dcparentzapp.constants;

import android.content.Context;
import android.provider.Settings;

/**
 * Created by razzl_000 on 10/11/2016.
 */
public class GlobalConstants {

    private static GlobalConstants instance = null;
    private String android_id;

    public static GlobalConstants getInstance(Context context) {
        if (instance == null)
            instance = new GlobalConstants(context);
        return instance;
    }

    private GlobalConstants(Context context) {
        android_id = Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }

    public String getAndroidId() {
        return android_id;
    }
}
