package com.dc.dcparentzapp;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.dc.dcparentzapp.connection.HttpClientManager;
import com.dc.dcparentzapp.constants.AppConstants;
import com.dc.dcparentzapp.fragment.BaseFragment;
import com.dc.dcparentzapp.model.GalleryInfo;
import com.dc.dcparentzapp.util.EncryptedSharedUtils;
import com.dc.dcparentzapp.util.SharedUtil;
import com.dc.dcparentzapp.util.Util;
import com.dms.datalayerapi.network.Http;
import com.dms.datalayerapi.util.GetUrlMaker;

import java.util.List;

/**
 * Created by raja on 17/07/17.
 */

public abstract class BaseActivity extends AppCompatActivity {


    public void startActivity(Class<?> nextActivity, boolean reqFinish) {
        Intent intent = new Intent(this, nextActivity);
        startActivity(intent);
        if (reqFinish)
            finish();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    protected void initFragments(Bundle savedInstanceState, Fragment... fragments) {
        if (savedInstanceState == null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.setCustomAnimations(R.anim.slide_in_from_top, R.anim.slide_out_to_top);
            for (Fragment fragment : fragments) {
                ft.add(R.id.container, fragment);
            }
            //ft.addToBackStack(null);
            ft.commit();
        }
    }

    public void hideActionBar() {
        getSupportActionBar().setShowHideAnimationEnabled(false);
        getSupportActionBar().hide();
    }

    public void showActionbarWithColor(int color, String title) {
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(color));
        getSupportActionBar().setTitle(title);
        getSupportActionBar().setShowHideAnimationEnabled(false);
        getSupportActionBar().show();

    }


    //
    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        List<Fragment> allFragments = getSupportFragmentManager().getFragments();
//        for (Fragment fragment : allFragments) {
//            if (fragment != null && fragment.isAdded()) {
//                ((BaseFragment) fragment).onBackPressed();
//            }
//        }
    }

    protected void requestNoTitle() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
    }


    protected void initToolBar(String headerName, int layoutId) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        int icon = getIntent().getIntExtra("icon", 0);
        int position = getIntent().getIntExtra("position", 0);
        if (icon != 0)
            ((ImageView) toolbar.findViewById(R.id.header_icon)).setImageResource(icon);

        TextView headerNameTextView = ((TextView) toolbar.findViewById(R.id.headerName));
        if (headerNameTextView != null)
            headerNameTextView.setText(headerName);

        toolbar.findViewById(R.id.header_icon_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Drawable drawable = ContextCompat.getDrawable(this, icon);
        PorterDuffColorFilter porterDuffColorFilter = new PorterDuffColorFilter(Color.WHITE,
                PorterDuff.Mode.SRC_ATOP);

        drawable.setColorFilter(porterDuffColorFilter);
        ((ImageView) toolbar.findViewById(R.id.header_icon)).setImageDrawable(drawable);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.findViewById(R.id.header_icon).setTransitionName("center_icon" + position);
        }

        ViewGroup rootView_layout = (ViewGroup) findViewById(R.id.rootView_layout);
        View rootLayout = getLayoutInflater().inflate(layoutId, null);
        rootView_layout.addView(rootLayout,
                new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

    }


    public <T> void fetchNUpdateDB(String baseURL, Class<T> t) {
        GetUrlMaker getUrlMaker = Util.getCommonGetPerms(BaseActivity.this);
        HttpClientManager client = HttpClientManager.get(this);
        client.diskCacheEnable(true);
        client.new NetworkTask<Void, T>(t, Http.GET) {
            @Override
            protected void onPostExecute(T t) {
                super.onPostExecute(t);
                if (t != null) {
                    onServiceFinish(t);
                } else {
                    Snackbar.make(getWindow().getDecorView().getRootView(), "Unable to connect to the remote server", Snackbar.LENGTH_LONG).show();
                }
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, getUrlMaker.getPathForGetUrl(baseURL));
    }

    protected <T> void onServiceFinish(T t) {
        updateOnDB(t);
    }

    protected <T> void updateOnDB(T t) {

    }

}
