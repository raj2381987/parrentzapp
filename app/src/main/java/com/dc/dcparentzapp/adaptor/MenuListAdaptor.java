package com.dc.dcparentzapp.adaptor;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dc.dcparentzapp.R;
import com.dc.dcparentzapp.model.MenuItem;
import com.dc.dcparentzapp.util.AnimatedConstents;
import com.dc.dcparentzapp.util.DisplayUtils;

import java.util.ArrayList;

/**
 * Created by rpeela on 10/16/15.
 */
public class MenuListAdaptor extends ArrayBaseAdapter {

    private final FragmentActivity context;
    private ArrayList<MenuItem> menuItems = new ArrayList<>();
    private int statusBarHeight;

    public MenuListAdaptor(FragmentActivity context, ArrayList<MenuItem> menuItems) {
        super(context, R.layout.menu_list_item);
        this.context = context;
        this.menuItems = menuItems;

        statusBarHeight = getStatusBarHeight(context);
    }

    @Override
    public int getCount() {
        return menuItems.size();
    }

    public int getStatusBarHeight(FragmentActivity fragmentActivity) {
        int result = 0;
        int resourceId = fragmentActivity.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = fragmentActivity.getResources().getDimensionPixelSize(resourceId);
        }
        return result - 11;// Padding 10 pixels
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rootView = layoutInflater.inflate(R.layout.menu_list_item, null);
        rootView.findViewById(R.id.menu_list_item_root).setBackgroundColor(menuItems.get(position).getBg_color());
        rootView.findViewById(R.id.menu_list_item_root).setMinimumHeight((int) ((DisplayUtils.get(context).height - statusBarHeight) / getCount()));
        AnimatedConstents.setListAnimation(rootView.findViewById(R.id.menu_list_item_root), position + 1, (int) DisplayUtils.get(context).height);
        ((TextView) rootView.findViewById(R.id.menu_text)).setText(menuItems.get(position).getLeft_menu_name());
        ((ImageView) rootView.findViewById(R.id.menu_icon)).setImageResource(menuItems.get(position).getLeft_menu_icon());
        return rootView;
    }


}
