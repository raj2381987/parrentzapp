package com.dc.dcparentzapp.adaptor;

import android.animation.Animator;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dc.dcparentzapp.R;
import com.dc.dcparentzapp.constants.AppConstants;
import com.dc.dcparentzapp.model.MenuItem;
import com.dc.dcparentzapp.util.AnimatedConstents;

import java.util.ArrayList;

/**
 * Created by raja on 26/07/17.
 */

public class DashBoardGridAdapter extends RecyclerView.Adapter<DashBoardGridAdapter.DataHolder> {

    private static String LOG_TAG = "MyRecyclerViewAdapter";
    private ArrayList<MenuItem> mDataset;
    private static MyClickListener myClickListener;
    private Activity context;
    private boolean lockedAnimations = false;
    private int lastAnimatedItem = -1;
    private static final int ANIMATION_DELAY = 600;


    public static class DataHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private View rootView;
        TextView label;
        View center_icon;

        public DataHolder(View itemView) {
            super(itemView);
            rootView = itemView;
            label = (TextView) itemView.findViewById(R.id.txt_item);
            center_icon = itemView.findViewById(R.id.img_item);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

        }
    }

    public void setOnItemClickListener(MyClickListener myClickListener) {
        this.myClickListener = myClickListener;
    }

    public DashBoardGridAdapter(Activity context, ArrayList<MenuItem> myDataset) {
        this.context = context;
        mDataset = myDataset;
    }

    @Override
    public DataHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.dashboard_grid_item, parent, false);
        DataHolder dataObjectHolder = new DataHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(final DataHolder holder, final int position) {
        holder.label.setText(mDataset.get(position).getLeft_menu_name());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            holder.center_icon.setTransitionName("center_icon" + position);
        }
        final int icon = mDataset.get(position).getLeft_menu_icon();
        ((ImageView) holder.center_icon).setImageResource(icon);
        holder.rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent;
                intent = new Intent(context, AppConstants.getActivityName(position));
                intent.putExtra("position", position);
                intent.putExtra("icon", icon);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    Pair<View, String> p1 = Pair.create(holder.center_icon, "center_icon" + position);
                    ActivityOptionsCompat aoc = ActivityOptionsCompat.makeSceneTransitionAnimation(context, p1);
                    context.startActivity(intent, aoc.toBundle());
                } else {
                    context.startActivity(intent);
                }
            }
        });
        animatePhoto(holder);
    }

    private void animatePhoto(final DataHolder viewHolder) {
        if (!lockedAnimations) {
            if (lastAnimatedItem == viewHolder.getLayoutPosition()) {
                setLockedAnimations(true);
            }
            final long animationDelay = ANIMATION_DELAY + viewHolder.getLayoutPosition() * 60;
            viewHolder.rootView.setScaleY(0);
            viewHolder.rootView.setScaleX(0);

            viewHolder.rootView.animate().setListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    AnimatedConstents.rotateAnimateIcon(viewHolder.center_icon, animationDelay);
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            }).scaleY(1)
                    .scaleX(1)
                    .setDuration(400)
                    .setInterpolator(AnimatedConstents.INTERPOLATOR)
                    .setStartDelay(animationDelay)
                    .start();
        }
    }


    public void addItem(MenuItem dataObj, int index) {
        mDataset.add(dataObj);
        notifyItemInserted(index);
    }

    public void deleteItem(int index) {
        mDataset.remove(index);
        notifyItemRemoved(index);
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public interface MyClickListener {
        public void onItemClick(int position, View v);
    }


    public void setLockedAnimations(boolean lockedAnimations) {
        this.lockedAnimations = lockedAnimations;
    }

}
