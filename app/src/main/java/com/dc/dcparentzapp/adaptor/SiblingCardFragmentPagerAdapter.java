package com.dc.dcparentzapp.adaptor;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v7.widget.CardView;
import android.view.ViewGroup;

import com.dc.dcparentzapp.db.DBUtil;
import com.dc.dcparentzapp.fragment.SiblingCardFragment;
import com.dc.dcparentzapp.model.Sibling;
import com.dc.dcparentzapp.util.SharedUtil;
import com.dc.dcparentzapp.util.Util;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by raja on 03/10/17.
 */

public class SiblingCardFragmentPagerAdapter extends FragmentStatePagerAdapter implements SiblingAdapter {

    private List<SiblingCardFragment> fragments;
    private float baseElevation;

    public SiblingCardFragmentPagerAdapter(Context context, FragmentManager fm, SiblingCardFragment.PositionChangeListener posListener) {
        super(fm);
        fragments = new ArrayList<>();
        this.baseElevation = Util.dpToPixels(2, context);
        ArrayList<Sibling.SiblingInfo> allSiblings = DBUtil.get(context).getAllValuesFromTable(Sibling.SiblingInfo.class, null);
        if (allSiblings.size() > 0) {
            for (int i = 0; i < allSiblings.size(); i++) {
                if (SharedUtil.get(context).getString(SharedUtil.KEY_ACTIVE_USER_ID) == null)
                    SharedUtil.get(context).store(SharedUtil.KEY_ACTIVE_USER_ID, allSiblings.get(i).SiblingId);
                SiblingCardFragment fragment = SiblingCardFragment.getInstance(i, true, allSiblings.get(i)._id);
                fragment.setPositionChangeListener(posListener);
                addCardFragment(fragment);
            }
        } else {
            addCardFragment(SiblingCardFragment.getInstance(0, false, 0));
        }

    }


    @Override
    public float getBaseElevation() {
        return baseElevation;
    }

    @Override
    public CardView getCardViewAt(int position) {
        return fragments.get(position).getCardView();
    }

    @Override
    public int getCount() {
        return fragments.size();
    }


    public SiblingCardFragment getSelectedFragment(int position) {
        return fragments.get(position);
    }

    public List<SiblingCardFragment> getAllFragments() {
        return fragments;
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Object fragment = super.instantiateItem(container, position);
        fragments.set(position, (SiblingCardFragment) fragment);
        return fragment;
    }

    public void addCardFragment(SiblingCardFragment fragment) {
        fragments.add(fragment);
    }

}
