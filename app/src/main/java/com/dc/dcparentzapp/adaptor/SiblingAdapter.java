package com.dc.dcparentzapp.adaptor;

import android.support.v7.widget.CardView;

/**
 * Created by raja on 03/10/17.
 */

public interface SiblingAdapter {

    public final int MAX_ELEVATION_FACTOR = 8;

    float getBaseElevation();

    CardView getCardViewAt(int position);

    int getCount();
}
