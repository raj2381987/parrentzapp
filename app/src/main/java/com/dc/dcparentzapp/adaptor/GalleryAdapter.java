package com.dc.dcparentzapp.adaptor;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.dc.dcparentzapp.FullScreenImageView;
import com.dc.dcparentzapp.R;
import com.dc.dcparentzapp.model.GalleryInfo;
import com.dc.dcparentzapp.util.GlideImageUtil;
import com.dc.dcparentzapp.util.Util;

import java.util.List;

/**
 * Created by raja on 03/11/17.
 */

public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.MyViewHolder> {

    private final Context context;
    private List<GalleryInfo.PhotoEvents> moviesList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView;

        public MyViewHolder(View view) {
            super(view);
            imageView = (ImageView) view.findViewById(R.id.gallery_image);
        }
    }


    public GalleryAdapter(Context context, List<GalleryInfo.PhotoEvents> moviesList) {
        this.context = context;
        this.moviesList = moviesList;
    }

    public void setMoviesList(List<GalleryInfo.PhotoEvents> moviesList) {
        this.moviesList = moviesList;
        notifyDataSetChanged();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.gallery_view_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final GalleryInfo.PhotoEvents photoEvents = moviesList.get(position);
        GlideImageUtil.get(context).setUrl(Util.recoverImageUrl(photoEvents.path)).setPlaceHolder(R.drawable.image_loading_place_holder).setErrorHolder(R.drawable.image_not_found).setImageView(holder.imageView).load();
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, FullScreenImageView.class);
                intent.putExtra("path", Util.recoverImageUrl(photoEvents.path));
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}
