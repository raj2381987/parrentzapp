package com.dc.dcparentzapp;

import android.graphics.Color;
import android.os.Bundle;

import com.dc.dcparentzapp.fragment.MenuFragment;

public class HomeActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestNoTitle();
        setContentView(R.layout.activity_home);
        initFragments(savedInstanceState);
    }

    void initFragments(Bundle savedInstance) {
        hideActionBar();
        super.initFragments(savedInstance, MenuFragment.getInstance());
        showActionbarWithColor(Color.RED, "Hello");
    }
}
