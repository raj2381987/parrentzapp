package com.dc.dcparentzapp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;

import com.dc.dcparentzapp.util.GlideImageUtil;
import com.dc.dcparentzapp.util.Util;

/**
 * Created by raja on 03/11/17.
 */

public class FullScreenImageView extends BaseActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fullimage_item);
        ImageView imgDisplay = (ImageView) findViewById(R.id.full_image);
        GlideImageUtil.get(this).setUrl(Util.recoverImageUrl(getIntent().getStringExtra("path"))).setPlaceHolder(R.drawable.image_loading_place_holder).setErrorHolder(R.drawable.image_not_found).setImageView(imgDisplay).load();
        findViewById(R.id.close_icon).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
