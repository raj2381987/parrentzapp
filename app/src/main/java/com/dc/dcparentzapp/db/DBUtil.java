package com.dc.dcparentzapp.db;

import android.content.Context;

import com.dc.dcparentzapp.model.GalleryInfo;
import com.dc.dcparentzapp.model.HolidaysRes;
import com.dc.dcparentzapp.model.LoginResponse;
import com.dc.dcparentzapp.model.Sibling;
import com.dms.datalayerapi.db.DBSupportUtil;

import java.util.ArrayList;


/**
 * Created by raja on 20/08/17.
 */

public class DBUtil extends DBSupportUtil {


    private static final int VERSION = 1;
    private static final String FILE_NAME = "parrentzApp.db";
    private static DBUtil instance = null;

    private DBUtil(Context context) {
        super(context);
    }

    @Override
    protected ArrayList<Class> getAllTables(ArrayList<Class> classes) {
        classes.add(HolidaysRes.Holiday.class);
        classes.add(HolidaysRes.Events.class);
        classes.add(GalleryInfo.PhotoEvents.class);
        classes.add(LoginResponse.StudentDetails.class);
        classes.add(LoginResponse.ParentProfile.class);
        classes.add(Sibling.SiblingInfo.class);
        return classes;
    }


    public static DBUtil get(Context context) {
        if (instance == null)
            instance = new DBUtil(context);
        return instance;
    }


    @Override
    protected String getDatabaseFileName() {
        return FILE_NAME;
    }

    @Override
    public int getDatabaseVersion() {
        return VERSION;
    }
}
