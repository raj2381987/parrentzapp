package com.dc.dcparentzapp;

import android.os.Bundle;

public class DigitalCampusCornerActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_activity);
        initGUI();
        initData();
    }

    private void initData() {

    }

    private void initGUI() {
        initToolBar("Digital Campus Corner", R.layout.content_view);
    }
}
