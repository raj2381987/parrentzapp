package com.dc.dcparentzapp;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.dc.dcparentzapp.connection.HttpClientManager;
import com.dc.dcparentzapp.constants.AppConstants;
import com.dc.dcparentzapp.db.DBUtil;
import com.dc.dcparentzapp.fragment.dialog.BaseFragmentDialog;
import com.dc.dcparentzapp.model.LoginResponse;
import com.dc.dcparentzapp.model.Sibling;
import com.dc.dcparentzapp.util.EncryptedSharedUtils;
import com.dc.dcparentzapp.util.SharedUtil;
import com.dc.dcparentzapp.util.Util;
import com.dms.datalayerapi.network.Http;
import com.dms.datalayerapi.util.GetUrlMaker;
import com.rengwuxian.materialedittext.MaterialEditText;

public class LoginActivity extends BaseActivity {

    private View p_icon, parrentz_icon;
    private View login_btn, signup_btn;
    private EditText accessCode, passCode, schoolCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initGUI();
    }

    private void initGUI() {
        parrentz_icon = findViewById(R.id.parrentz_icon);
        p_icon = findViewById(R.id.p_icon);
        login_btn = findViewById(R.id.login_btn);
        signup_btn = findViewById(R.id.signup_btn);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            findViewById(R.id.parrentz_icon).setTransitionName("parrentz_icon");
            findViewById(R.id.p_icon).setTransitionName("p_icon");
        }
        initValues();
        login_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doLogin();
            }
        });
        signup_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Util.hideKeyBoard(this);
    }

    private void initValues() {
        accessCode = (MaterialEditText) findViewById(R.id.access_code);
        passCode = (MaterialEditText) findViewById(R.id.pass_code);
        schoolCode = (EditText) findViewById(R.id.school_code);


        schoolCode.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    doLogin();
                    return true;
                }
                return false;
            }
        });

        login_btn.setFocusable(true);
    }

    private void doLogin() {

        GetUrlMaker getUrlMaker = GetUrlMaker.getMaker().addParams("username", accessCode.getText().toString())
                .addParams("password", passCode.getText().toString()).addParams("schoolcode", schoolCode.getText().toString());
//                .addParams("regid", GlobalConstants.getInstance(this).getAndroidId())
//                .addParams("SENDER_ID", GlobalConstants.getInstance(this).getAndroidId())
//                .addParams("API_KEY", GlobalConstants.getInstance(this).getAndroidId())
//                .addParams("IMEI", GlobalConstants.getInstance(this).getAndroidId());

        HttpClientManager client = HttpClientManager.get(this);
        client.diskCacheEnable(false);
        final BaseFragmentDialog dialog = Util.showBaseLoading(this);
        client.new NetworkTask<Void, LoginResponse>(LoginResponse.class, Http.GET) {
            @Override
            protected void onPostExecute(LoginResponse loginData) {
                super.onPostExecute(loginData);
                dialog.dismiss();
                if (loginData != null) {
                    if (loginData.StudentInfo != null && loginData.StudentInfo.size() > 0) {
                        DBUtil.get(LoginActivity.this).dropTable(LoginResponse.Studentinfo.class);
                        DBUtil.get(LoginActivity.this).bulkInsertion(loginData.StudentInfo);
                        DBUtil.get(LoginActivity.this).dropTable(LoginResponse.ParentProfile.class);
                        if (loginData.StudentInfo.get(0).studentDetails != null)
                            DBUtil.get(LoginActivity.this).removeAndInsert(loginData.StudentInfo.get(0).studentDetails);
                        onSuccess(loginData.StudentInfo.get(0).studentDetails.schoollogo);
                    }
                } else {
                    Snackbar.make(getWindow().getDecorView().getRootView(), "invalid inputs", Snackbar.LENGTH_LONG).show();
                }
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, getUrlMaker.getPathForGetUrl(AppConstants.LOGIN_URL));
    }

    private void onSuccess(String schoollogo) {
        EncryptedSharedUtils.get(this).putValue(SharedUtil.KEY_ACCESS_CODE, accessCode.getText().toString());
        EncryptedSharedUtils.get(this).putValue(SharedUtil.KEY_PASS_CODE, passCode.getText().toString());
        EncryptedSharedUtils.get(this).putValue(SharedUtil.KEY_SCHOOL_CODE, schoolCode.getText().toString());
        SharedUtil.get(this).store(SharedUtil.IS_LOGIN, true);
        if (schoollogo != null && !schoollogo.contains("http"))
            schoollogo = "http://" + schoollogo;
        SharedUtil.get(this).store(SharedUtil.SCHOOL_LOGO, schoollogo.replace(" ", "%20"));
        initSiblings();
    }

    private void initSiblings() {
        GetUrlMaker getUrlMaker = GetUrlMaker.getMaker().addParams("username", EncryptedSharedUtils.get(this).getValue(SharedUtil.KEY_ACCESS_CODE))
                .addParams("password", EncryptedSharedUtils.get(this).getValue(SharedUtil.KEY_PASS_CODE))
                .addParams("schoolcode", EncryptedSharedUtils.get(this).getValue(SharedUtil.KEY_SCHOOL_CODE));

        HttpClientManager client = HttpClientManager.get(this);
        client.diskCacheEnable(true);
        client.new NetworkTask<Void, Sibling>(Sibling.class, Http.GET) {
            @Override
            protected void onPostExecute(Sibling sibling) {
                super.onPostExecute(sibling);
                if (sibling != null) {
                    DBUtil.get(LoginActivity.this).dropTable(Sibling.SiblingInfo.class);
                    if (sibling.siblings != null && sibling.siblings.size() > 0) {
                        DBUtil.get(LoginActivity.this).bulkInsertion(sibling.siblings);
                    }
                    moveToNextActivity();
                } else {
                    Snackbar.make(getWindow().getDecorView().getRootView(), "Unable to connect to the remote server", Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            protected Sibling manipulateMoreOnBackGround(Sibling classType) {
                if (classType != null) {
                    if (classType.siblings != null && classType.siblings.size() > 0) {
                        Sibling.SiblingInfo siblingInfo = DBUtil.get(LoginActivity.this).getValuesFromTable("isActiveSibling = 1", Sibling.SiblingInfo.class);
                        if (siblingInfo != null) {
                            for (int i = 0; i < classType.siblings.size(); i++) {
                                if (classType.siblings.get(i).SiblingId.equalsIgnoreCase(siblingInfo.SiblingId)) {
                                    classType.siblings.get(i).isActiveSibling = true;
                                    break;
                                }
                            }
                        } else {
                            classType.siblings.get(0).isActiveSibling = true;
                        }
                    }
                }
                return classType;
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, getUrlMaker.getPathForGetUrl(AppConstants.GET_SIBLING));
    }

    private void moveToNextActivity() {
        Intent intent = new Intent(this, DashBoard1.class);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Pair<View, String> p1 = Pair.create(parrentz_icon, "parrentz_icon");
            Pair<View, String> p2 = Pair.create(p_icon, "p_icon");
            ActivityOptionsCompat aoc = ActivityOptionsCompat.makeSceneTransitionAnimation(this, p1, p2);
            startActivity(intent, aoc.toBundle());
        } else {
            startActivity(intent);
        }
        finish();
    }
}
