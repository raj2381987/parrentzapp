package com.dc.dcparentzapp.model;

/**
 * Created by rpeela on 10/16/15.
 */
public class MenuItem {
    private String left_menu_name;
    private int left_menu_icon;

    private int bg_color;

    public MenuItem(String name, int icon, int bg_color) {
        this.left_menu_icon = icon;
        this.left_menu_name = name;
        this.bg_color = bg_color;
    }

    public String getLeft_menu_name() {
        return left_menu_name;
    }

    public void setLeft_menu_name(String left_menu_name) {
        this.left_menu_name = left_menu_name;
    }

    public int getLeft_menu_icon() {
        return left_menu_icon;
    }

    public void setLeft_menu_icon(int left_menu_icon) {
        this.left_menu_icon = left_menu_icon;
    }


    public int getBg_color() {
        return bg_color;
    }

}
