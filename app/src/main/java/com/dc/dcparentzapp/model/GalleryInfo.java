package com.dc.dcparentzapp.model;

import java.util.ArrayList;

/**
 * Created by raja on 03/11/17.
 */

public class GalleryInfo {

    public ArrayList<Galleries> StudentInfo;

    public static class Galleries {
        public String studentId;
        public ArrayList<Gallery> gallery;
    }

    public static class Gallery {
        public int _id;
        public ArrayList<PhotoEvents> eventPhotos;
        public String eventName;
    }

    public static class PhotoEvents {
        public String eventName;
        public String studentId;
        public String path;
        public String title;
    }
}
