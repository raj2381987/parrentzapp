package com.dc.dcparentzapp.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by raja on 31/08/17.
 */

public class LoginResponse {
    public ArrayList<Studentinfo> StudentInfo;


    public static class Studentinfo {
        public String studentId;
        public StudentDetails studentDetails;
    }

    public static class StudentDetails {
        public String gender, studentName, studentImage, section, smsNumber;
        @SerializedName("class")
        public String _class;
        public ArrayList<ParentProfile> parentProfile;
        public ArrayList<StudentAddress> studentAddress;
        public String schoollogo;
    }

    public static class StudentAddress {
        public String country, pin, city, permanentAddress, state, schoolName;
    }


    public static class Studentattendance {
        @SerializedName("roll no")
        public String rollno;
        @SerializedName("total present days")
        public String totalpresentdays;
        @SerializedName("from")
        public String fromDate;
        @SerializedName("to")
        public String toDate;
        @SerializedName("total working days")
        public String totalworkingdays;
        @SerializedName("class")
        public String _class;
        public ArrayList<Attendance> attendance;

        public static class Attendance {
            @SerializedName("present days")
            public String presentdays;
            public String month;
            @SerializedName("working days")
            public String workingdays;

        }
    }

    public static class ParentProfile {
        public String fatherName, motherNationality,
                fatherOccupation, fatherQualification, fatherDesignation,
                motherName, motherDesignation, motherEmail, fatherEmail,
                motherOccupation, motherQualification, fatherNationality;

        @SerializedName("motherMobile no")
        public String fatherMobile;
        @SerializedName("fatherMobile no")
        public String motherMobile;

    }


    public static class Transport {
        @SerializedName("Route Name")
        public String routeName;
        @SerializedName("Vehicle Identity")
        public String vehicleIdentity;
        @SerializedName("Route Details")
        public ArrayList<RouteDetail> routeDetails;
        public String Term;
        @SerializedName("Pick Up Point")
        public String pickUpPoint;

    }


    public static class RouteDetail {
        @SerializedName("Arrival Time")
        public String arrivalTime;
        @SerializedName("Starting Time")
        public String startingTime;
        @SerializedName("Pick Up Point")
        public String pickUpPoint;
        public String Distance;

    }

    public static class Mes {
        public String Breakfast;
        public String Dinner;
        public String Snaks;
        public String Day;
        public String Lunch;

    }


}