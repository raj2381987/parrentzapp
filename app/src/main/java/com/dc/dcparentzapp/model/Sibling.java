package com.dc.dcparentzapp.model;

import java.util.ArrayList;

/**
 * Created by raja on 07/10/17.
 */

public class Sibling {

    public ArrayList<SiblingInfo> siblings;

    public static class SiblingInfo {
        public String SiblingName;
        public String SiblingId;
        public String Gender;
        public String SiblingImage;
        public String SchoolLogo;
        public boolean isActiveSibling;
        public int _id;
    }
}
