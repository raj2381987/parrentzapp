package com.dc.dcparentzapp.model;

import android.accounts.Account;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by raja on 03/11/17.
 */

public class PaymentInfo {

    public List<StudentInfo> StudentInfo;


    public static class Details {
        public String Amount;

        public String Fine;

        @SerializedName("Paid Amt")
        public String Paid_Amt;

        @SerializedName("Fee Name")
        public String Fee_Name;

        @SerializedName("Last Date")
        public String Last_Date;

        public String Pending;


    }

    public class Fee {
        @SerializedName("total fine")
        public String total_fine;

        public String Category;

        @SerializedName("total paid amount")
        public String total_paid_amount;

        @SerializedName("total pending")
        public String total_pending;

        public String clas;

        @SerializedName("Adm No")
        public String Adm_No;

        public List<Details> details;

        @SerializedName("total amount")
        public String total_amount;

        public String Name;


    }

    public class PercentageTaxes {
        public String paymentmode;

        public int convenience_tax;

        public int service_tax;


    }

    public class ALL {
        public String total;

        public String fee_concession;

        public String due;

        public String fine;

        public String feename;

        public String feeid;

        public String payamount;

        public String lastdate;


    }

    public class ParentFee {
        public List<String> Receipt;

        public List<Fee> fee;

        public String name;

        public List<PercentageTaxes> percentage_taxes;

        public String payu;

        public String student_id;

        public String rollno;

        public String section;

        public String admno;

        public String _class;

        public List<Account> account;


    }


    public class StudentInfo {
        public String studentId;

        public List<ParentFee> parentFee;

    }

}
