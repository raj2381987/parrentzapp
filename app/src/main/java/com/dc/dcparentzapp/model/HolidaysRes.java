package com.dc.dcparentzapp.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by raja on 06/09/17.
 */

public class HolidaysRes {
    private ArrayList<Calander> calander;

    public ArrayList<Calander> getCalander() {
        return calander;
    }

    public void setCalander(ArrayList<Calander> calander) {
        this.calander = calander;
    }

    public static class Calander {
        private ArrayList<Holiday> holiday;
        private ArrayList<Events> events;

        public ArrayList<Holiday> getHoliday() {
            return holiday;
        }

        public void setHoliday(ArrayList<Holiday> holiday) {
            this.holiday = holiday;
        }

        public ArrayList<Events> getEvents() {
            return events;
        }

        public void setEvents(ArrayList<Events> events) {
            this.events = events;
        }

        @Override
        public String toString() {
            return "ClassPojo [holiday = " + holiday + ", events = " + events + "]";
        }
    }


    public static class Events {
        private String endtime;

        private String eventDate;

        private String starttime;

        private String description;

        private String recurring;

        private String eventname;

        public String getEndtime() {
            return endtime;
        }

        public void setEndtime(String endtime) {
            this.endtime = endtime;
        }

        public String getEventDate() {
            return eventDate;
        }

        public void setEventDate(String eventDate) {
            this.eventDate = eventDate;
        }

        public String getStarttime() {
            return starttime;
        }

        public void setStarttime(String starttime) {
            this.starttime = starttime;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getRecurring() {
            return recurring;
        }

        public void setRecurring(String recurring) {
            this.recurring = recurring;
        }

        public String getEventname() {
            return eventname;
        }

        public void setEventname(String eventname) {
            this.eventname = eventname;
        }

    }

    public static class Holiday {
        private String Name;
        private String To;
        private String Holiday;

        @SerializedName("No of Days")
        private String no_of_Days;

        public String getName() {
            return Name;
        }

        public void setName(String Name) {
            this.Name = Name;
        }

        public String getTo() {
            return To;
        }

        public void setTo(String To) {
            this.To = To;
        }

        public String getHoliday() {
            return Holiday;
        }

        public void setHoliday(String Holiday) {
            this.Holiday = Holiday;
        }

        public String getNoofDays() {
            return no_of_Days;
        }

        public void setNoofDays(String no_of_Days) {
            this.no_of_Days = no_of_Days;
        }

    }
}
