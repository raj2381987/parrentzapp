package com.dc.dcparentzapp;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.view.View;

import com.dc.dcparentzapp.util.SharedUtil;

public class Splash extends BaseActivity {

    private View imageView;
    private View p_icon;
    private Boolean isLogin = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        isLogin = SharedUtil.get(this).getBoolean(SharedUtil.IS_LOGIN);

        initGUI();
        startThread();
    }

    private void initGUI() {
        imageView = findViewById(R.id.parrentz_icon);
        p_icon = findViewById(R.id.p_icon);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            imageView.setTransitionName("parrentz_icon");
            p_icon.setTransitionName("p_icon");
        }
    }

    private void startThread() {
        new Handler().postDelayed(new Runnable() {
            public void run() {
                Intent intent;
                if (!isLogin)
                    intent = new Intent(Splash.this, LoginActivity.class);
                else
                    intent = new Intent(Splash.this, DashBoard1.class);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    Pair<View, String> p1 = Pair.create(imageView, "parrentz_icon");
                    Pair<View, String> p2 = Pair.create(p_icon, "p_icon");
                    ActivityOptionsCompat aoc = ActivityOptionsCompat.makeSceneTransitionAnimation(Splash.this, p1, p2);
                    startActivity(intent, aoc.toBundle());
                } else {
                    startActivity(intent);
                }
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        finish();
                    }
                }, 1000);
            }
        }, 4000);

    }
}
