package com.dc.dcparentzapp.custom;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by raja on 03/11/17.
 */

public class AttendanceInfo {

    public List<StudentInfo> StudentInfo;

    public class Attendance {
        public String month;

        public String workingdays;

        public String presentdays;
    }

    public class Studentattendance {
        @SerializedName("roll no")
        public String roll_no;

        public String totalpresentdays;

        public String from;

        public String totalworkingdays;

        public String to;

        public String _class;

        public List<Attendance> attendance;


    }

    public class StudentInfo {
        public String studentId;

        public List<Studentattendance> studentattendance;

        public List<String> attendancedate;


    }

}
