package com.dc.dcparentzapp.custom;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.dc.dcparentzapp.R;


/**
 * Created by raja on 28/06/17.
 */

@SuppressLint("AppCompatCustomView")
public class PercentageImageView extends ImageView {
    private float heightRatio;

    public PercentageImageView(Context context) {
        this(context, null);
    }

    public PercentageImageView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PercentageImageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.PercentageImageView, 0, 0);
        try {
            heightRatio = a.getFloat(R.styleable.PercentageImageView_imageHeightRatio, 0f);
        } finally {
            a.recycle();
        }
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        if (heightRatio != 0 && widthMode == MeasureSpec.EXACTLY && heightMode != MeasureSpec.EXACTLY) {
            int width = MeasureSpec.getSize(widthMeasureSpec);
            int height = MeasureSpec.getSize(heightMeasureSpec);
            if (heightRatio != 0) {
                height = (int) (heightRatio * width);
            }
            setMeasuredDimension(width, height);
        } else {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }
    }

    public float getHeightRatio() {
        return heightRatio;
    }
}
