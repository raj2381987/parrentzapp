package com.dc.dcparentzapp.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dc.dcparentzapp.R;
import com.dc.dcparentzapp.adaptor.SiblingAdapter;
import com.dc.dcparentzapp.db.DBUtil;
import com.dc.dcparentzapp.model.LoginResponse;
import com.dc.dcparentzapp.model.Sibling;
import com.dc.dcparentzapp.util.GlideImageUtil;
import com.dc.dcparentzapp.util.SharedUtil;

/**
 * Created by raja on 03/10/17.
 */

public class SiblingCardFragment extends Fragment {
    private CardView cardView;
    private TextView studentName;
    private View checked;
    private Sibling.SiblingInfo sibling;

    public static SiblingCardFragment getInstance(int position, boolean isFromDB, int _id) {
        SiblingCardFragment f = new SiblingCardFragment();
        Bundle args = new Bundle();
        args.putInt("_id", _id);
        args.putBoolean("isFromDB", isFromDB);
        args.putInt("position", position);
        f.setArguments(args);
        return f;
    }

    @SuppressLint("DefaultLocale")
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.sibling_card_item, container, false);
        cardView = (CardView) view.findViewById(R.id.cardView);
        studentName = (TextView) view.findViewById(R.id.studentName);
        checked = view.findViewById(R.id.checked);
        cardView.setMaxCardElevation(cardView.getCardElevation() * SiblingAdapter.MAX_ELEVATION_FACTOR);
        ImageView imageView = (ImageView) view.findViewById(R.id.student_image);
        boolean isFromDB = getArguments().getBoolean("isFromDB", false);
        int _id = getArguments().getInt("_id");
        final int position = getArguments().getInt("position");

        String url = "";
        if (isFromDB) {
            sibling = DBUtil.get(getActivity()).getValuesFromTable("_id=" + _id, Sibling.SiblingInfo.class);
            studentName.setText(sibling.SiblingName);
            url = sibling.SiblingImage.replace(" ", "%20");
            if (sibling.Gender.equalsIgnoreCase("MALE")) {
                imageView.setImageResource(R.drawable.profile_man);
            } else {
                imageView.setImageResource(R.drawable.profile_female);
            }
        } else {
            LoginResponse.StudentDetails studentInfo = DBUtil.get(getActivity()).getValuesFromTable(null, LoginResponse.StudentDetails.class);
            if (studentInfo != null)
                url = studentInfo.studentImage != null ? studentInfo.studentImage.replace(" ", "%20") : "";
        }
        if (!url.startsWith("http"))
            url = "http://" + url;
        if (url.equalsIgnoreCase("")) {

        } else {
            try {
                GlideImageUtil.get(getActivity()).setScaleType(GlideImageUtil.ScaleType.TYPE_FULL_SCREEN).setUrl(url).setImageView(imageView).load();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (positionChangeListener != null)
                    positionChangeListener.onPositionChanged(position);
            }
        });

        return view;
    }

    public CardView getCardView() {
        return cardView;
    }

    public void setEnabled(boolean isEnabled) {
        if (getActivity() != null && checked != null) {
            checked.setVisibility(isEnabled ? View.VISIBLE : View.GONE);
            if (sibling != null) {
                String siblingId = sibling.SiblingId;
                SharedUtil.get(getActivity()).store(SharedUtil.KEY_ACTIVE_USER_ID, siblingId);
            }
        }
    }

    private PositionChangeListener positionChangeListener;

    public void setPositionChangeListener(PositionChangeListener positionChangeListener) {
        this.positionChangeListener = positionChangeListener;
    }

    public interface PositionChangeListener {
        void onPositionChanged(int position);
    }
}
