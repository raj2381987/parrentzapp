package com.dc.dcparentzapp.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dc.dcparentzapp.R;

/**
 * Created by raja on 17/07/17.
 */

public class ContentFragment extends BaseFragment {


    public static ContentFragment getInstance(int position) {
        ContentFragment instance = new ContentFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("currentFragmentPosition", position);
        instance.setArguments(bundle);
        return instance;
    }

    public ContentFragment() {
        super();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        currentFragmentPosition = getArguments().getInt("currentFragmentPosition");
        View rootView = inflater.inflate(R.layout.content_fragment, container, false);

        return rootView;
    }
}

