package com.dc.dcparentzapp.fragment;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.View;

import com.dc.dcparentzapp.BaseActivity;
import com.dc.dcparentzapp.R;
import com.dc.dcparentzapp.constants.AppConstants;

/**
 * Created by raja on 17/07/17.
 */

public class BaseFragment extends Fragment {

    private static final int DAILOG_FRAGMENT = 0x000012;
    private static final int APP_EXIT_DAILOG_FRAGMENT = 0x00014;
    protected int currentFragmentPosition = 0;

    public BaseFragment() {

    }

    protected void addNewFragment(Fragment fragment, String fragmentName) {
        FragmentConstents.addFragmenttoDisplay(getActivity(), fragment, fragmentName);
    }


    protected void addMenuFragment(Fragment origen) {
        ((BaseActivity) getActivity()).hideActionBar();
        FragmentConstents.removeFragment(getFragmentManager(), origen, R.anim.slide_out_to_bottom, R.anim.slide_out_to_bottom);
        FragmentConstents.addFragment(getFragmentManager(), MenuFragment.getInstance(), R.anim.slide_in_from_top, R.anim.slide_out_to_top);
    }


    public void onBackPressed() {
        View rootView = getView().findViewById(R.id.rootView);
        FragmentManager fm = getFragmentManager();
//        addMenuFragment(this);
        String tag = "";
        if (rootView != null) {
            addMenuFragment(this);
        } else {
            getActivity().finish();
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (!MenuFragment.class.isInstance(this)) {
            ((BaseActivity) getActivity()).showActionbarWithColor(AppConstants.MENU_ITEM_COLORS[currentFragmentPosition], AppConstants.MENU_ITEMS[currentFragmentPosition]);
            ((BaseActivity) getActivity()).getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
            ((BaseActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }else{
            ((BaseActivity) getActivity()).showActionbarWithColor(Color.parseColor("#801f59"), "");
            ((BaseActivity) getActivity()).getSupportActionBar().hide();

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case DAILOG_FRAGMENT:
                if (resultCode == Activity.RESULT_OK) {
                    addMenuFragment(this);
                }
//                if (dialogFragment != null)
//                    dialogFragment.dismiss();
                break;
            case APP_EXIT_DAILOG_FRAGMENT:
                if (resultCode == Activity.RESULT_OK) {
                    getActivity().finish();
                }
//                if (dialogFragment != null)
//                    dialogFragment.dismiss();
                break;
        }
    }
}
