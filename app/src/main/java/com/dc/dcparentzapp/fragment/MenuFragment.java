package com.dc.dcparentzapp.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.dc.dcparentzapp.R;
import com.dc.dcparentzapp.adaptor.MenuListAdaptor;
import com.dc.dcparentzapp.constants.AppConstants;

/**
 * Created by rpeela on 10/16/15.
 */
public class MenuFragment extends BaseFragment {

    public static MenuFragment instance = null;
    MenuListAdaptor adaptor;

    public static MenuFragment getInstance() {
        if (instance == null)
            instance = new MenuFragment();
        return instance;
    }

    public MenuFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.menu_fragment, container, false);
        ListView listView = (ListView) rootView.findViewById(R.id.menu_list);
        adaptor = new MenuListAdaptor(getActivity(), AppConstants.getLeftMenuElements(getActivity()));
        listView.setAdapter(adaptor);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                FragmentConstents.addFragmentFromBottom(getActivity(), AppConstants.getPositionFragment(position));
                FragmentConstents.removeFragment(getFragmentManager(), MenuFragment.this, R.anim.slide_out_to_top, R.anim.slide_out_to_top);
            }
        });
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
}
